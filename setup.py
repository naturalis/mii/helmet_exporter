import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="helmet_exporter",
    version="0.0.1",
    author="Naturalis Biodiversity Center",
    author_email="support@naturalis.nl",
    description="Helmet metric exporter for Prometheus",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/naturalis/mii/helmet_exporter",
    packages=setuptools.find_packages(),
    install_requires=['prometheus_client'],
    scripts=['helmet_exporter.py'],
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: Unix",
    ),
)
