#!/bin/sh
python3 setup.py install -f
cp /opt/helmet-exporter/helmet-exporter.service /etc/systemd/system/
chmod 644 /etc/systemd/system/helmet-exporter.service
if [ ! -f /opt/helmet-exporter/helmet.env ]; then
    cp /opt/helmet-exporter/helmet.env.dist /opt/helmet-exporter/helmet.env
    chmod 644 /opt/helmet-exporter/helmet.env
fi
systemctl daemon-reload
systemctl enable helmet-exporter
systemctl start helmet-exporter
