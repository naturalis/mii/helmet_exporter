# helmet_export

Een helm metric exporter voor Prometheus. Dit is een python proces
dat via tcp luistert op ingestelde poort. De
[Helmet Bluetooth Hub]https://gitlab.com/naturalis/mii/helmet-bluetooth-hub)
ontvangt bluetooth json records van ieder helmpje en stuur die gegevens
door naar deze service. De records worden toegevoegd aan de
metrics zodat die gescraped kunnen worden door de Prometheus server.

De waarden van deze service staan in `helmet.env` en moeten overeen komen
met de waarden in de hub, het netwerk en de prometheus server.
