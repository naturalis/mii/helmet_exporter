#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from prometheus_client import start_http_server, Summary, CollectorRegistry
from prometheus_client.core import Counter, Gauge, REGISTRY
from systemd import journal
import time
import socket
import json
import sys
import serial
import os

metrics = {}
cristals = ['Toermalijn', 'Prehniet', 'Bariet', 'Fluoriet', 'Zwavel', 'Goud', 'Willemiet', 'Apatiet', 'Sodaliet', 'Bergkristal', 'Pyriet', 'Aragoniet', 'Calciet', 'Okeniet']

def log(msg=''):
    print(msg)
    journal.send(msg)

def collect(data = False):
    log(json.dumps(data))
    if (data):
        prefix = "helmet%02d" % (data.get('HelmetNr',0))

        if (data.get('BatteryPerc')):
            batterykey ='%s_battery_ratio' % (prefix)
            if (not metrics.get(batterykey)) :
                metrics[batterykey] = Gauge(
                      batterykey,
                      'Battery Percentage of Helmet'
                      )
            metrics[batterykey].set(data.get('BatteryPerc',0) * 0.01)

        if (data.get('ChargerTemp')):
            chargerkey ='%s_charger_celcius' % (prefix)
            if (not metrics.get(chargerkey)) :
                metrics[chargerkey] = Gauge(chargerkey,
                      'Temperature of the charger')
            metrics[chargerkey].set(data.get('ChargerTemp',0))

        if (data.get('BatteryVoltage')):
            voltagekey ='%s_battery_volts' % (prefix)
            if (not metrics.get(voltagekey)) :
                metrics[voltagekey] = Gauge(voltagekey,
                      'Voltage level of the battery')
            metrics[voltagekey].set(data.get('BatteryVoltage',0))

        if (data.get('Objects')):
            objects = data.get('Objects')
            hitskey = '%s_treasure_hits' % (prefix)
            if (not metrics.get(hitskey)):
                metrics[hitskey] = Counter(hitskey,
                      'Counting the number of hits of cristals found')
            metrics[hitskey].inc(sum(objects))

            uniqkey = '%s_treasure_success' % (prefix)
            if (not metrics.get(uniqkey)):
                metrics[uniqkey] = Counter(uniqkey,
                      'Counting the number of unique cristals found')

            uniqcount = 0
            for i, o in enumerate(objects):
                if (o > 0):
                    uniqcount =+ 1
                    cristal = cristals[i]
                    if (not metrics.get(cristal)):
                        metrics[cristal] = Counter(cristal,
                                'Counting "%s" found' % (cristal))

                    metrics[cristal].inc(o)

            metrics[uniqkey].inc(uniqcount)


if __name__ == '__main__':
    try:
        port = os.environ.get('UDP_PORT',9111)
        ip = os.environ.get('UDP_IP','')
    except Exception:
        log("Please set the UDP listening port.")
        sys.exit(1)

    try:
        sock = socket.socket(
                    socket.AF_INET, # Internet
                    socket.SOCK_STREAM)
        sock.bind((ip, int(port)))
        sock.listen(1)
        log("listening!")
    except Exception as e:
        log("Cannot listen to TCP port")
        log(e)
        sys.exit(1)

    # Start up the server to expose the metrics.
    log("starting metrics server")
    metrics_port=os.environ.get('METRICS_PORT', 8000)
    start_http_server(int(metrics_port))
    log("metrics server started")

    # Register HelmetCollector
    while True:
        log("accepting connections")
        connection, client_address = sock.accept()

        while True:
            try:
                line = connection.recv(512)
            except socket.error:
                log(socket.error)
                break

            if not line: break

            try:
                data = json.loads(line.decode().strip())
            except:
                log("Could not parse json data")
                break

            if (data):
                collect(data)

        connection.close()
